designModule = angular.module "design"

designModule.controller "Design3Controller", [
  "$scope"
  ($scope) ->

    $scope.content =
      data : [
        "There can be more &gt;1 element"
        "See?"
        "<b>This one is bold</b>"
        "<i>This one is italic</i>"
      ]

    $scope.addNewItem = (index) ->
      $scope.content.data.splice(index + 1, 0, "default text")

    $scope.removeItem = (index) ->
      $scope.content.data.splice(index, 1)

]
