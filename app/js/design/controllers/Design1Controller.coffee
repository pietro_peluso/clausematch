designModule = angular.module "design"

designModule.controller "Design1Controller", [
  "$scope"
  "$timeout"
  ($scope
   $timeout) ->

    $scope.content =
      data : [
        "<i>Hello</i> world...<br><br>This is some content<br><br>Isn't it awesome?<br><br>I like it..."
      ]

    $scope.splitData = ->
      $scope.splittedData = $scope.content.data[0].split "<br><br>"
      $scope.splittedData

    $scope.onTextChange = (index, text) ->
      $scope.splittedData[index] = text
      $scope.content.data[0] = $scope.splittedData.join("<br><br>")

    $scope.addNewItem = ->
      if $scope.splittedData.length is 0 #it should never happen but we want to make it robust
          $scope.content.data[0] += "<br><br>New default text added<br><br>"
      else
        $scope.content.data[0] += "<br><br>New default text added"

    $scope.removeItem = ->
      if $scope.splittedData.length > 1
        $scope.splittedData.pop()
        $scope.content.data[0] = $scope.splittedData.join("<br><br>")

]
