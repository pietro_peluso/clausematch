angular.module('clausematch', [
  'templates'
  'ngSanitize'
  'ui.router'
  'ngRepeatReorder'
  'design'
]).config(['$locationProvider', ($location) -> $location.html5Mode(false)

]).config(['$stateProvider', ($stateProvider) ->
  $stateProvider
  .state('home',
    url: '/'
    public: true
    templateUrl: 'views/home.html'
    title: 'Home'

  ).state('design1',
    url: '/design1'
    public: true
    templateUrl: 'views/design1.html'
    controller: 'Design1Controller'
    title: 'Design 1'

  ).state('design2',
    url: '/design2'
    public: true
    templateUrl: 'views/design2.html'
    controller: 'Design2Controller'
    title: 'Design 2'

  ).state('design3',
    url: '/design3'
    public: true
    templateUrl: 'views/design3.html'
    controller: 'Design3Controller'
    title: 'Design 3'
  )

]).run ['$rootScope', '$state', ($rootScope, $state) ->
  $rootScope.title = 'ClauseMatch'
  $state.transitionTo 'home'
]
