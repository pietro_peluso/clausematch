# ClauseMatch

### Tech

This application uses a number of open source projects to work properly:

* [AngularJS] - HTML enhanced for web apps!
* [Twitter Bootstrap] - Great UI boilerplate for modern web apps
* [Grunt] - The JavaScript task runner
* [Bower] - A package manager for the web
* [jQuery] - duh

### Installation

Execute the following commands from you terminal/shell:

```sh
$ git clone https://pietro_peluso@bitbucket.org/pietro_peluso/clausematch.git
$ cd clausematch
$ npm install
$ bower install
$ grunt dist
$ cd dist
$ open index.html
```

License
----
MIT

[AngularJS]:http://angularjs.org
[Twitter Bootstrap]:http://twitter.github.com/bootstrap/
[jQuery]:http://jquery.com
[Grunt]:http://gruntjs.com
[Bower]:http://bower.io
