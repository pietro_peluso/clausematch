module.exports = function(grunt){
    grunt.initConfig({
        devFolder: 'app',
        distFolder: 'dist',
        renderedJs: 'renderedJs',
        vendorFolder: '<%= devFolder %>/bower_components',
        pkg: grunt.file.readJSON('package.json'),

        copy: {
            index: {
                expand: true,
                cwd: '<%= devFolder %>',
                src: 'index.html',
                dest: '<%= distFolder %>'
            },
            html5shiv : {
                expand : true,
                cwd: '<%= vendorFolder %>/html5shiv/dist',
                src: 'html5shiv.min.js',
                dest: '<%= distFolder %>/vendor/html5shiv'
            }
        },

        cssmin : {
            add_banner: {
                options: {
                    banner: '/* <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */'
                },
                files: {
                    '<%= distFolder %>/css/main.css': ['<%= distFolder %>/css/main.css']
                }
            }
        },

        coffee :{
            compile : {
                expand: true,
                cwd: '<%= devFolder %>/js',
                src: ['*.coffee', '*/*.coffee','*/**/*.coffee'],
                dest: '<%= renderedJs %>',
                ext: '.js'
            }
        },

        concat: {
            vendorJs : {
                options: {
                    separator : ';'
                },
                src: [
                    '<%= vendorFolder %>/jquery/dist/jquery.js',
                    '<%= vendorFolder %>/angular/angular.js',
                    '<%= vendorFolder %>/angular-sanitize/angular-sanitize.js',
                    '<%= vendorFolder %>/angular-ui-router/release/angular-ui-router.js',
                    '<%= vendorFolder %>/hammerjs/hammer.js',
                    '<%= vendorFolder %>/angular-hammer/angular-hammer.js',
                    '<%= vendorFolder %>/ngRepeatReorder/dist/ngRepeatReorder.js',
                    '<%= vendorFolder %>/bootstrap/dist/js/bootstrap.js'
                ],

                dest: '<%= distFolder %>/js/vendor.js'
            },
            js : {
                options: {
                    separator : ';'
                },
                src : ['<%= renderedJs %>/*.js', '<%= renderedJs %>/*/*.js','<%= renderedJs %>/*/**/*.js'],
                dest: '<%= distFolder %>/js/main.js'
            },
            final : {
                options: {
                    separator : ';'
                },
                src: ['<%= distFolder %>/js/vendor.js', '<%= distFolder %>/js/main.js'],
                dest: '<%= distFolder %>/js/main.js'
            },
            css : {
                src: [
                    '<%= vendorFolder %>/bootstrap/dist/css/bootstrap.css',
                    '<%= devFolder %>/css/main.css'
                ],
                dest: '<%= distFolder %>/css/main.css'
            }
        },

        uglify: {
            build: {
                src: '<%= distFolder %>/js/main.js',
                dest: '<%= distFolder %>/js/main.js'
            }
        },

        clean : {
            project: [
                "<%= distFolder %>/js/main.js",
                "<%= distFolder %>/css/main.css",
                "<%= renderedJs %>/js/templates/templates/template.js"
            ],
            docs : [
                "docs"
            ],
            docBuild : [
                '<%= renderedJs %>'
            ]
        },

        watch : {
            project : {
                files : [
                    '<%= devFolder %>/css/*.css',
                    '<%= devFolder %>/js/**/*.coffee',
                    '<%= devFolder %>/layout/**/*.html',
                    '<%= devFolder %>/views/**/*.html',
                    '<%= devFolder %>/index.html'
                ],
                tasks : ['copy:index', 'coffee', 'prangler', 'concat:vendorJs', 'concat:js', 'concat:final', 'concat:css']
            }
        },

        prangler: {
            default: {
                options: {
                    ngApp: 'templates', // name of your angular module
                    stripPathForTemplateId: 'app/', // will remove src from the $templcateCache key
                    stripFilenameExtension: false, // if true removes .html from $templateCache key
                    filenameForTemplateId: false // if true template loaded by filename

                },
                files: {
                    '<%= renderedJs %>/templates/templates/template.js': [
                        '<%= devFolder %>/views/**/*.html',
                        '<%= devFolder %>/layout/**/*.html'
                    ]
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-concat-css');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-angular-prangler');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-coffee');

    grunt.registerTask('dev', ['clean:project', 'copy', 'coffee', 'prangler', 'concat:vendorJs', 'concat:js', 'concat:final',  'concat:css', 'watch:project']);
    grunt.registerTask('dist', ['clean:project', 'copy', 'coffee', 'prangler', 'concat', 'uglify', 'cssmin']);
};
